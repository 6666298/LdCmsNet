﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Basics
{
    using LdCms.EF.DbContext;
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Basics;
    using LdCms.IDAL.Basics;
    using LdCms.Common.Json;
    public partial class AreaService:BaseService<Ld_Basics_Area>,IAreaService
    {
        private readonly IAreaDAL AreaDAL;
        public AreaService(IAreaDAL AreaDAL)
        {
            this.AreaDAL = AreaDAL;
            this.Dal = AreaDAL;
        }
        public override void SetDal()
        {
            Dal = AreaDAL;
        }

        public List<Ld_Basics_Area> GetArea(int systemId, int cityId)
        {
            try
            {
                return FindList(m => m.SystemID == systemId && m.CityId == cityId).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
