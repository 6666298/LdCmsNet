﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Basics
{
    using LdCms.EF.DbContext;
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Basics;
    using LdCms.IDAL.Basics;
    using LdCms.Common.Json;
    /// <summary>
    /// 
    /// </summary>
    public partial class MediaInterfaceService:BaseService<Ld_Basics_MediaInterface>,IMediaInterfaceService
    {
        private readonly IMediaInterfaceDAL MediaInterfaceDAL;
        public MediaInterfaceService(IMediaInterfaceDAL MediaInterfaceDAL)
        {
            this.MediaInterfaceDAL = MediaInterfaceDAL;
            this.Dal = MediaInterfaceDAL;
        }
        public override void SetDal()
        {
            Dal = MediaInterfaceDAL;
        }



    }
}
