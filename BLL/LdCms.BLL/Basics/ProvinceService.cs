﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Basics
{
    using LdCms.EF.DbContext;
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Basics;
    using LdCms.IDAL.Basics;
    using LdCms.Common.Json;
    public partial class ProvinceService:BaseService<Ld_Basics_Province>,IProvinceService
    {
        private readonly IProvinceDAL ProvinceDAL;
        public ProvinceService(IProvinceDAL ProvinceDAL)
        {
            this.ProvinceDAL = ProvinceDAL;
            this.Dal = ProvinceDAL;
        }
        public override void SetDal()
        {
            Dal = ProvinceDAL;
        }

        public List<Ld_Basics_Province> GetProvince(int systemId)
        {
            try
            {
                return FindList(m => m.SystemID == systemId).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



    }
}
