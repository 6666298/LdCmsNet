﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Institution
{
    
    using LdCms.EF.DbModels;
    
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Institution;
    using LdCms.IDAL.Institution;
    using LdCms.Common.Json;
    using LdCms.Common.Extension;

    public partial class StaffRefreshTokenService:BaseService<Ld_Institution_StaffRefreshToken>,IStaffRefreshTokenService
    {
        private readonly IStaffRefreshTokenDAL StaffRefreshTokenDAL;
        
        public StaffRefreshTokenService(IStaffRefreshTokenDAL StaffRefreshTokenDAL)
        {
            
            this.StaffRefreshTokenDAL = StaffRefreshTokenDAL;
            this.Dal = StaffRefreshTokenDAL;
        }
        public override void SetDal()
        {
            Dal = StaffRefreshTokenDAL;
        }



    }
}
