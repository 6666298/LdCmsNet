﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Institution
{
    using LdCms.EF.DbModels;
    
    using LdCms.EF.DbStoredProcedure;
    
    using LdCms.IBLL.Institution;
    using LdCms.IDAL.Institution;
    using LdCms.Common.Json;
    public partial class SupplierService:BaseService<Ld_Institution_Supplier>,ISupplierService
    {
        private readonly ISupplierDAL SupplierDAL;
        
        public SupplierService(ISupplierDAL SupplierDAL)
        {
            
            this.SupplierDAL = SupplierDAL;
            this.Dal = SupplierDAL;
        }
        public override void SetDal()
        {
            Dal = SupplierDAL;
        }

    }
}
