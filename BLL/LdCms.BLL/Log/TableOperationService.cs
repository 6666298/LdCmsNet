﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Log
{
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Log;
    using LdCms.IDAL.Log;
    using LdCms.Common.Extension;
    using LdCms.Common.Json;
    using LdCms.Common.Utility;

    /// <summary>
    /// 
    /// </summary>
    public partial class TableOperationService:BaseService<Ld_Log_TableOperation>,ITableOperationService
    {
        private readonly ITableOperationDAL TableOperationDAL;
        public TableOperationService(ITableOperationDAL TableOperationDAL)
        {
            this.TableOperationDAL = TableOperationDAL;
            this.Dal = TableOperationDAL;
        }
        public override void SetDal()
        {
            Dal = TableOperationDAL;
        }

        public bool IsTableOperation(long id)
        {
            return IsExists(m => m.ID == id);
        }

        public bool SaveTableOperation(Ld_Log_TableOperation entity)
        {
            try
            {
                entity.CreateDate = DateTime.Now;
                return Add(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool SaveTableOperation(Ld_Log_TableOperation entity, out long id)
        {
            try
            {
                entity.CreateDate = DateTime.Now;
                bool result = Add(entity);
                id = result ? entity.ID : 0;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool UpdateTableOperationState(long id, bool state)
        {
            try
            {
                if (!IsTableOperation(id))
                    throw new Exception("id invalid！");
                var entity = GetTableOperation(id);
                entity.State = state;
                return Update(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteTableOperation(long id)
        {
            try
            {
                return Delete(m => m.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Ld_Log_TableOperation GetTableOperation(long id)
        {
            try
            {
                return Find(m => m.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Ld_Log_TableOperation> GetTableOperationTop(int count)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_TableOperation>();
                expression = expression.And(m => true);
                int total = Utility.ToTopTotal(count);
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_TableOperation> GetTableOperationPaging(int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_TableOperation>();
                expression = expression.And(m => true);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.CreateDate, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_TableOperation> SearchTableOperation(string startTime, string endTime, string clientId, string classId, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                int intClientId = clientId.ToByte();
                int intClassId = classId.ToByte();

                var expression = ExtLinq.True<Ld_Log_TableOperation>();
                expression = expression.And(m => m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == intClassId)
                && (string.IsNullOrEmpty(classId) ? true : m.ClientID.Value == intClassId)
                && (m.TableName.Contains(keyword) || m.Account.Contains(keyword)));

                int total = Utility.ToTopTotal(count);
                return FindListTop(expression, m => m.ID, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int CountTableOperation()
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_TableOperation>();
                expression = expression.And(m => true);
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountTableOperation(string startTime, string endTime, string clientId, string classId, string keyword)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                int intClientId = clientId.ToByte();
                int intClassId = classId.ToByte();

                var expression = ExtLinq.True<Ld_Log_TableOperation>();
                expression = expression.And(m => m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == intClassId)
                && (string.IsNullOrEmpty(classId) ? true : m.ClientID.Value == intClassId)
                && (m.TableName.Contains(keyword) || m.Account.Contains(keyword)));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
