﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Member
{
    using LdCms.EF.DbModels;
    
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Member;
    using LdCms.IDAL.Member;
    /// <summary>
    /// 
    /// </summary>
    public partial class AccountRefreshTokenService:BaseService<Ld_Member_AccountRefreshToken>,IAccountRefreshTokenService
    {
        private readonly IAccountRefreshTokenDAL AccountRefreshTokenDAL;
        
        public AccountRefreshTokenService(IAccountRefreshTokenDAL AccountRefreshTokenDAL)
        {
            
            this.AccountRefreshTokenDAL = AccountRefreshTokenDAL;
            this.Dal = AccountRefreshTokenDAL;
        }
        public override void SetDal()
        {
            Dal = AccountRefreshTokenDAL;
        }

    }
}
