﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Member
{
    using LdCms.EF.DbModels;
    
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Member;
    using LdCms.IDAL.Member;
    using LdCms.Common.Extension;
    using LdCms.Common.Security;
    using LdCms.Common.Utility;

    /// <summary>
    /// 
    /// </summary>
    public partial class AddressService:BaseService<Ld_Member_Address>,IAddressService
    {
        private readonly IAddressDAL AddressDAL;
        
        public AddressService(IAddressDAL AddressDAL)
        {
            
            this.AddressDAL = AddressDAL;
            this.Dal = AddressDAL;
        }
        public override void SetDal()
        {
            Dal = AddressDAL;
        }

        public bool SaveAddress(Ld_Member_Address entity)
        {
            try
            {
                int systemId = entity.SystemID;
                string companyId = entity.CompanyID;
                string addressId = entity.AddressID;
                bool isDefault = entity.IsDefault.ToBool();
                bool state = entity.State.ToBool();
                if (string.IsNullOrEmpty(addressId))
                {
                    var primarykey = PrimaryKeyHelper.PrimaryKeyType.MemberAddress;
                    var primaryKeyLen = PrimaryKeyHelper.PrimaryKeyLen.V1;
                    entity.AddressID = PrimaryKeyHelper.MakePrimaryKey(primarykey, primaryKeyLen);
                }
                entity.IsDefault = isDefault;
                entity.State = state;
                entity.CreateDate = DateTime.Now;
                int intnum = 0;
                var dbContext = new DAL.BaseDAL();
                using (var db = dbContext.DbEntities())
                {
                    using (var trans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (isDefault)
                            {
                                var expression = ExtLinq.True<Ld_Member_Address>();
                                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                                var lists = FindList(expression).ToList();
                                lists.ForEach(m => m.IsDefault = false);
                                dbContext.Update(lists);
                            }
                            dbContext.Add(entity);
                            intnum = db.SaveChanges();
                            trans.Commit();
                        }
                        catch (Exception)
                        {
                            trans.Rollback();
                        }
                        return intnum > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool UpdateAddress(Ld_Member_Address entity)
        {
            try
            {
                int systemId = entity.SystemID;
                string companyId = entity.CompanyID;
                string addressId = entity.AddressID;
                bool isDefault = entity.IsDefault.ToBool();
                bool state = entity.State.ToBool();
                entity.IsDefault = isDefault;
                entity.State = state;
                entity.CreateDate = DateTime.Now;
                int intnum = 0;
                var dbContext = new DAL.BaseDAL();
                using (var db = dbContext.DbEntities())
                {
                    using (var trans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (isDefault)
                            {
                                var expression = ExtLinq.True<Ld_Member_Address>();
                                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.AddressID != addressId);
                                var lists = FindList(expression).ToList();
                                lists.ForEach(m => m.IsDefault = false);
                                dbContext.Update(lists);
                            }
                            dbContext.Update(entity);
                            intnum = db.SaveChanges();
                            trans.Commit();
                        }
                        catch (Exception)
                        {
                            trans.Rollback();
                        }
                        return intnum > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool UpdateAddressDefault(int systemId, string companyId, string addressId, bool isDefault)
        {
            try
            {
                var entity = Find(m => m.SystemID == systemId && m.CompanyID == companyId && m.AddressID == addressId);
                if (entity == null)
                    throw new Exception("platform id invalid！");
                entity.IsDefault = isDefault;

                int intnum = 0;
                var dbContext = new DAL.BaseDAL();
                using (var db = dbContext.DbEntities())
                {
                    using (var trans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (isDefault)
                            {
                                var expression = ExtLinq.True<Ld_Member_Address>();
                                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.AddressID != addressId);
                                var lists = FindList(expression).ToList();
                                lists.ForEach(m => m.IsDefault = false);
                                dbContext.Update(lists);
                            }
                            dbContext.Update(entity);
                            intnum = db.SaveChanges();
                            trans.Commit();
                        }
                        catch (Exception)
                        {
                            trans.Rollback();
                        }
                        return intnum > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool UpdateAddressState(int systemId, string companyId, string addressId, bool state)
        {
            try
            {
                var entity = Find(m => m.SystemID == systemId && m.CompanyID == companyId && m.AddressID == addressId);
                if (entity == null)
                    throw new Exception("invoice id invalid！");
                entity.State = state;
                return Update(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteAddress(int systemId, string companyId, string addressId)
        {
            try
            {
                return Delete(m => m.SystemID == systemId && m.CompanyID == companyId && m.AddressID == addressId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Ld_Member_Address GetAddress(int systemId, string companyId, string addressId)
        {
            try
            {
                return Find(m => m.SystemID == systemId && m.CompanyID == companyId && m.AddressID == addressId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Ld_Member_Address GetAddressByDefault(int systemId, string companyId)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_Address>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.IsDefault == true);
                return FindList(expression, m => m.CreateDate, true).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_Address> GetAddressAll(int systemId, string companyId)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_Address>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                return FindList(expression, m => m.IsDefault, false).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_Address> GetAddressTop(int systemId, string companyId, int count)
        {
            try
            {
                int total = Utility.ToTopTotal(count);
                var expression = ExtLinq.True<Ld_Member_Address>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_Address> GetAddressPaging(int systemId, string companyId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_Address>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.CreateDate, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_Address> SearchAddress(int systemId, string companyId, string startTime, string endTime, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                int total = Utility.ToTopTotal(count);
                //条件
                var expression = ExtLinq.True<Ld_Member_Address>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (m.MemberID.Contains(keyword) || m.Name.Contains(keyword) || m.Tags.Contains(keyword)|| m.Address.Contains(keyword)));
                //执行
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_Address> GetAddressPagingByMemberId(int systemId, string companyId, string memberId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_Address>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.MemberID == memberId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.CreateDate, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int CountAddress(int systemId, string companyId)
        {
            try
            {
                return Count(m => m.SystemID == systemId && m.CompanyID == companyId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountAddress(int systemId, string companyId, string memberId)
        {
            try
            {
                return Count(m => m.SystemID == systemId && m.CompanyID == companyId && m.MemberID == memberId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountAddress(int systemId, string companyId, string startTime, string endTime, string keyword)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                var expression = ExtLinq.True<Ld_Member_Address>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (m.MemberID.Contains(keyword) || m.Name.Contains(keyword) || m.Tags.Contains(keyword) || m.Address.Contains(keyword)));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



    }
}
