﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Member
{
    using LdCms.EF.DbModels;
    
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Member;
    using LdCms.IDAL.Member;
    /// <summary>
    /// 
    /// </summary>
    public partial class AmountRecordService:BaseService<Ld_Member_AmountRecord>,IAmountRecordService
    {
        private readonly IAmountRecordDAL AmountRecordDAL;
        
        public AmountRecordService(IAmountRecordDAL AmountRecordDAL)
        {
            
            this.AmountRecordDAL = AmountRecordDAL;
            this.Dal = AmountRecordDAL;
        }
        public override void SetDal()
        {
            Dal = AmountRecordDAL;
        }

    }
}
