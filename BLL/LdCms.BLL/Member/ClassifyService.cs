﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Member
{
    
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Member;
    using LdCms.IDAL.Member;
    using LdCms.Common.Extension;
    using LdCms.Common.Json;
    using LdCms.Common.Security;
    using LdCms.Common.Time;
    public partial class ClassifyService:BaseService<Ld_Member_Classify>,IClassifyService
    {
        private readonly IClassifyDAL ClassifyDAL;
        
        public ClassifyService(IClassifyDAL ClassifyDAL)
        {
            
            this.ClassifyDAL = ClassifyDAL;
            this.Dal = ClassifyDAL;
        }
        public override void SetDal()
        {
            Dal = ClassifyDAL;
        }

        public bool IsExists(int systemId, string companyId, int classId)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_Classify>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.ClassID == classId);
                return IsExists(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool SaveClassify(Ld_Member_Classify entity)
        {
            try
            {
                int systemId = entity.SystemID;
                string companyId = entity.CompanyID;
                int classId = entity.ClassID.ToInt();

                int intnum = 0;
                var dbContext = new DAL.BaseDAL();
                var db = dbContext.DbEntities();
                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (classId == 0)
                        {
                            var expression = ExtLinq.True<Ld_Member_Classify>();
                            expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                            var lists = FindListTop(expression, m => m.ClassID, false, 1);
                            entity.ClassID = lists.FirstOrDefault().ClassID + 1;
                        }
                        entity.CreateDate = DateTime.Now;
                        dbContext.Add(entity);
                        intnum = db.SaveChanges();
                        trans.Commit();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                    }
                    return intnum > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool UpdateClassify(Ld_Member_Classify entity)
        {
            try
            {
                entity.CreateDate = DateTime.Now;
                return Update(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool UpdateClassifyState(int systemId, string companyId, int classId, bool state)
        {
            try
            {
                if (classId == 1)
                    throw new Exception("初始化类别不可变更状态！");
                var entity = GetClassify(systemId, companyId, classId);
                if (entity == null)
                    throw new Exception("class id invalid！");
                entity.State = state.ToBool();
                return Update(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteClassify(int systemId, string companyId, int classId)
        {
            try
            {
                if (classId == 1)
                    throw new Exception("初始化类别不可变更状态！");
                var expression = ExtLinq.True<Ld_Member_Classify>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.ClassID == classId);
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteClassify(int systemId, string companyId, int[] classId)
        {
            try
            {
                if (classId.Contains(1))
                    throw new Exception("初始化类别不可变更状态！");
                var expression = ExtLinq.True<Ld_Member_Classify>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && classId.Contains(m.ClassID));
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Ld_Member_Classify GetClassify(int systemId, string companyId, int classId)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_Classify>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.ClassID == classId);
                return Find(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_Classify> GetClassify(int systemId, string companyId)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_Classify>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                return FindList(expression, m => m.ClassID, true).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_Classify> GetClassify(int systemId, string companyId, bool state)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_Classify>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.State.Value == state);
                return FindList(expression, m => m.ClassID, true).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountClassify(int systemId, string companyId)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_Classify>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountClassify(int systemId, string companyId, bool state)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_Classify>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.State.Value == state);
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
