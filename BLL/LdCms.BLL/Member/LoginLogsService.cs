﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Member
{
    using LdCms.Common.Extension;
    using LdCms.Common.Json;
    using LdCms.Common.Security;
    using LdCms.Common.Time;
    
    using LdCms.EF.DbStoredProcedure;
    using LdCms.EF.DbModels;
    using LdCms.IBLL.Member;
    using LdCms.IDAL.Member;
    using LdCms.Common.Utility;

    /// <summary>
    /// 
    /// </summary>
    public partial class LoginLogsService:BaseService<Ld_Member_LoginLogs>,ILoginLogsService
    {
        private readonly ILoginLogsDAL LoginLogsDAL;
        
        public LoginLogsService(ILoginLogsDAL LoginLogsDAL)
        {
            
            this.LoginLogsDAL = LoginLogsDAL;
            this.Dal = LoginLogsDAL;
        }
        public override void SetDal()
        {
            Dal = LoginLogsDAL;
        }

        public bool SaveLoginLogs(Ld_Member_LoginLogs entity)
        {
            try
            {
                int systemId = entity.SystemID.ToInt();
                string companyId = entity.CompanyID;
                string memberId = entity.MemberID;
                string ipAddress = entity.IpAddress;

                int intnum = 0;
                var dbContext = new DAL.BaseDAL();
                var db = dbContext.DbEntities();
                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var expression = ExtLinq.True<Ld_Member_Account>();
                        expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.MemberID == memberId);
                        var member = dbContext.Find(expression);
                        member.LastLoginIpAddress = ipAddress;
                        member.LastLoginTime = DateTime.Now;
                        entity.CreateDate = DateTime.Now;
                        dbContext.Add(entity);
                        dbContext.Update(member);
                        intnum = db.SaveChanges();
                        trans.Commit();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                    }
                    return intnum > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteLoginLogs(long id)
        {
            try
            {
                return Delete(m => m.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteLoginLogsAll(int systemId,string companyId)
        {
            try
            {
                DateTime time = DateTime.Now.AddDays(-3);
                var expression = ExtLinq.True<Ld_Member_LoginLogs>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.CreateDate.Value.Date <= time.Date);
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Ld_Member_LoginLogs GetLoginLogs(long id)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_LoginLogs>();
                expression = expression.And(m => m.ID == id);
                return Find(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_LoginLogs> GetLoginLogsTop(int systemId, string companyId, int count)
        {
            try
            {
                int total = Utility.ToTopTotal(count);
                var expression = ExtLinq.True<Ld_Member_LoginLogs>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_LoginLogs> GetLoginLogsPaging(int systemId, string companyId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_LoginLogs>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.CreateDate, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_LoginLogs> SearchLoginLogs(int systemId, string companyId, string startTime, string endTime, string clientId, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                int intClientId = clientId.ToInt();
                var expression = ExtLinq.True<Ld_Member_LoginLogs>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == intClientId)
                && (m.MemberID.Contains(keyword) || m.Account.Contains(keyword)));
                int total = Utility.ToTopTotal(count);
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountLoginLogs(int systemId,string companyId)
        {
            try
            {
                return Count(m => m.SystemID == systemId && m.CompanyID == companyId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
