﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Member
{
    using LdCms.EF.DbModels;
    
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Member;
    using LdCms.IDAL.Member;
    using LdCms.Common.Utility;
    using LdCms.Common.Extension;
    

    /// <summary>
    /// 
    /// </summary>
    public partial class PointRecordService:BaseService<Ld_Member_PointRecord>,IPointRecordService
    {
        private readonly IPointRecordDAL PointRecordDAL;
        
        public PointRecordService(IPointRecordDAL PointRecordDAL)
        {
            
            this.PointRecordDAL = PointRecordDAL;
            this.Dal = PointRecordDAL;
        }
        public override void SetDal()
        {
            Dal = PointRecordDAL;
        }


        public bool SavePointRecord(Ld_Member_PointRecord entity)
        {
            try
            {
                entity.State = true;
                entity.CreateDate = DateTime.Now;
                return Add(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Ld_Member_PointRecord GetPointRecord(int systemId, int id)
        {
            try
            {
                return Find(m => m.SystemID == systemId && m.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_PointRecord> GetPointRecordTop(int systemId, string companyId, int count)
        {
            try
            {
                int total = Utility.ToTopTotal(count);
                var expression = ExtLinq.True<Ld_Member_PointRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_PointRecord> GetPointRecordPaging(int systemId, string companyId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_PointRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.CreateDate, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_PointRecord> SearchPointRecord(int systemId, string companyId, string startTime, string endTime, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                int total = Utility.ToTopTotal(count);
                //条件
                var expression = ExtLinq.True<Ld_Member_PointRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (m.MemberID.Contains(keyword) || m.TasksID.Contains(keyword)));
                //执行
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Member_PointRecord> GetPointRecordPagingByMemberId(int systemId, string companyId, string memberId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Member_PointRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.MemberID == memberId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.CreateDate, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountPointRecord(int systemId, string companyId)
        {
            try
            {
                return Count(m => m.SystemID == systemId && m.CompanyID == companyId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountPointRecord(int systemId, string companyId, string memberId)
        {
            try
            {
                return Count(m => m.SystemID == systemId && m.CompanyID == companyId && m.MemberID == memberId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountPointRecord(int systemId, string companyId, string startTime, string endTime, string keyword)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                var expression = ExtLinq.True<Ld_Member_PointRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (m.MemberID.Contains(keyword) || m.TasksID.Contains(keyword)));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #region SP
        public bool AddPointRecordPro(int systemId, string companyId, string memberId, int classId, string className, int typeId, string typeName, int points, string tasksId, string body, string account, string nickname, string ipAddress, string remark, bool state)
        {
            try
            {
                int errCode = -1;
                string errMsg = "fail";
                int result = LdCmsDbEntitiesContext.SP_Add_Member_PointRecord(systemId, companyId, memberId, classId, className, typeId, typeName, points, tasksId, body, account, nickname, ipAddress, remark, state, out errCode, out errMsg);
                if (errCode != 0)
                    throw new Exception(errMsg);
                return errCode == 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


    }
}
