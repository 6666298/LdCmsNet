﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Service
{
    using LdCms.EF.DbModels;
    
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Service;
    using LdCms.IDAL.Service;
    using LdCms.Common.Json;
    using LdCms.Common.Security;
    using LdCms.Common.Extension;
    using LdCms.Common.Utility;
    /// <summary>
    /// 
    /// </summary>
    public partial class MessageBoardService:BaseService<Ld_Service_MessageBoard>,IMessageBoardService
    {
        private readonly IMessageBoardDAL MessageBoardDAL;
        
        public MessageBoardService(IMessageBoardDAL MessageBoardDAL)
        {
            
            this.MessageBoardDAL = MessageBoardDAL;
            this.Dal = MessageBoardDAL;
        }
        public override void SetDal()
        {
            Dal = MessageBoardDAL;
        }

        public bool SaveMessageBoard(Ld_Service_MessageBoard entity)
        {
            try
            {
                string messageId = PrimaryKeyHelper.MakePrimaryKey(PrimaryKeyHelper.PrimaryKeyType.ServiceMessageBoard);
                entity.MessageID = messageId;
                entity.IsTop = false;
                entity.State = false;
                entity.CreateDate = DateTime.Now;
                return Add(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteMessageBoard(int systemId, string companyId, string messageId)
        {
            try
            {
                return Delete(m => m.SystemID == systemId && m.CompanyID == companyId && m.MessageID == messageId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool UpdateMessageBoardState(int systemId, string companyId, string messageId, bool state)
        {
            try
            {
                var entity = GetMessageBoard(systemId, companyId, messageId);
                if (entity == null)
                    throw new Exception("not data！");
                entity.State = state;
                return Update(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool UpdateMessageBoardReply(int systemId, string companyId, string messageId, string reply, string replyStaffId, string replyStaffName, bool state)
        {
            try
            {
                var entity = GetMessageBoard(systemId, companyId, messageId);
                if (entity == null)
                    throw new Exception("not data！");
                entity.Reply = reply;
                entity.ReplyStaffId = replyStaffId;
                entity.ReplyStaffName = replyStaffName;
                entity.ReplyTime = DateTime.Now;
                entity.State = state;
                return Update(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Ld_Service_MessageBoard GetMessageBoard(int systemId, string companyId, string messageId)
        {
            try
            {
                return Find(m => m.SystemID == systemId && m.CompanyID == companyId && m.MessageID == messageId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Service_MessageBoard> GetMessageBoardTop(int systemId, string companyId, int count)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Service_MessageBoard>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                return FindListTop(expression, m => m.CreateDate, false, count).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Service_MessageBoard> GetMessageBoardPaging(int systemId, string companyId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Service_MessageBoard>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.CreateDate, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Service_MessageBoard> GetMessageBoardPaging(int systemId, string companyId, string state, int pageId, int pageSize)
        {
            try
            {
                bool blnState = state.ToBool();
                var expression = ExtLinq.True<Ld_Service_MessageBoard>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && (string.IsNullOrEmpty(state) ? true : m.State.Value == blnState));
                return FindListPaging(expression, m => m.CreateDate, false, pageId, pageSize).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Service_MessageBoard> SearchMessageBoard(int systemId, string companyId, string startTime, string endTime, string state, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime); 
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                bool blnState = state.ToBool();
                int total = Utility.ToTopTotal(count);
                var expression = ExtLinq.True<Ld_Service_MessageBoard>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(state) ? true : m.State.Value == blnState)
                && (m.MemberID.Contains(keyword) || m.Phone.Contains(keyword) || m.Name.Contains(keyword)));
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountMessageBoard(int systemId, string companyId)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Service_MessageBoard>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountMessageBoard(int systemId, string companyId, string state)
        {
            try
            {
                bool blnState = state.ToBool();
                var expression = ExtLinq.True<Ld_Service_MessageBoard>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && (string.IsNullOrEmpty(state) ? true : m.State.Value == blnState));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
