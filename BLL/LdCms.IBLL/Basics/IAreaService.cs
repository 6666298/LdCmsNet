﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Basics
{
    using LdCms.EF.DbModels;
    public partial interface IAreaService:IBaseService<Ld_Basics_Area>
    {
        List<Ld_Basics_Area> GetArea(int systemId, int cityId);

    }
}
