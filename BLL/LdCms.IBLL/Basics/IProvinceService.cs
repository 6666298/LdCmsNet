﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Basics
{
    using LdCms.EF.DbModels;
    public partial interface IProvinceService:IBaseService<Ld_Basics_Province>
    {
        List<Ld_Basics_Province> GetProvince(int systemId);
    }
}
