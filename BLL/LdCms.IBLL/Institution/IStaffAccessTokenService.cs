﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Institution
{
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    public partial interface IStaffAccessTokenService:IBaseService<Ld_Institution_StaffAccessToken>
    {
        bool SaveStaffAccessTokenPro(string accessToken, string refreshToken, int systemId, string companyId, string staffId, string platformId, int expiresIn, int refreshTokenExpiresIn, string ipAddress, int createTimestamp);
        bool SaveStaffRefreshTokenPro(string verifyRefreshToken, string accessToken, string refreshToken, int expiresIn, int refreshTokenExpiresIn, string ipAddress, int createTimestamp);
        bool VerifyStaffAccessTokenPro(string token, int timestamp);
    }
}
