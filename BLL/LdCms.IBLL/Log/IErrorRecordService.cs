﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Log
{
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    public partial interface IErrorRecordService:IBaseService<Ld_Log_ErrorRecord>
    {
        bool SaveErrorRecord(Ld_Log_ErrorRecord entity);
        bool DeleteErrorRecord(long id);
        bool DeleteErrorRecord(int systemId);

        Ld_Log_ErrorRecord GetErrorRecord(long id);
        List<Ld_Log_ErrorRecord> GetErrorRecordTop(int systemId, int count);
        List<Ld_Log_ErrorRecord> GetErrorRecordPaging(int systemId, int pageId, int pageSize);
        List<Ld_Log_ErrorRecord> SearchErrorRecord(int systemId, string startTime, string endTime, string clientId, string keyword, int count);

        int CountErrorRecord(int systemId);
        int CountErrorRecord(int systemId, string startTime, string endTime, string clientId, string keyword);

    }
}
