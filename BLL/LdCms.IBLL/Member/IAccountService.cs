﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Member
{
    using EF.DbModels;
    using LdCms.EF.DbStoredProcedure;

    /// <summary>
    /// 会员帐号业务逻辑服务类
    /// </summary>
    public partial interface IAccountService:IBaseService<Ld_Member_Account>
    {
        bool IsExists(int systemId, string companyId, string memberId);

        Ld_Member_Account GetAccount(int systemId, string companyId, string memberId);
        Ld_Member_Account GetAccountByUserName(int systemId, string companyId, string username);
        Ld_Member_Account GetAccountByPhone(int systemId, string companyId, string phone);
        List<Ld_Member_Account> GetAccountPaging(int systemId, string companyId, int pageId, int pageSize);
        List<Ld_Member_Account> GetAccountPaging(int systemId, string companyId, int classId, int pageId, int pageSize);
        List<Ld_Member_Account> GetAccountPaging(int systemId, string companyId, int classId, string rankId, int pageId, int pageSize);



        bool SaveAccount(Ld_Member_Account entity);
        bool UpdateAccount(Ld_Member_Account entity);
        bool UpdateAccountState(int systemId, string companyId, string memberId, bool state);
        bool UpdateAccountDelete(int systemId, string companyId, string memberId, bool del);
        bool DeleteAccount(int systemId, string companyId, string memberId);
        int DeleteAccountAll(int systemId, string companyId, string memberId);

        int CountAccount(int systemId, string companyId);
        int CountAccount(int systemId, string companyId, int classId);
        int CountAccount(int systemId, string companyId, int classId, string rankId);



        bool SaveAccountRegisterPro(int systemId, string companyId, string memberId, int classId, string className, string userName, string password, string name, string phone, string ipAddress);
        bool UpdateAccountStatePro(int systemId, string companyId, string memberId, bool state);
        bool UpdateAccountDeletePro(int systemId, string companyId, string memberId, bool delete);
        bool UpdateAccountPasswordPro(int systemId, string companyId, string memberId, string password);
        bool UpdateAccountPhonePro(int systemId, string companyId, string memberId, string phone);
        bool DeleteAccountPro(int systemId, string companyId, string memberId);

        Ld_Member_Account GetAccountPro(int systemId, string companyId, string memberId);
        Ld_Member_Account GetAccountByAccessTokenPro(int systemId, string accessToken);
        Ld_Member_Account GetAccountByRefreshTokenPro(int systemId, string refreshToken);
        List<Ld_Member_Account> GetAccountTopPro(int systemId, string companyId, int count, out int rowCount);
        List<Ld_Member_Account> GetAccountTopPro(int systemId, string companyId, string delete, int count, out int rowCount);
        List<Ld_Member_Account> GetAccountPagingPro(int systemId, string companyId, int pageId, int pageSize, out int rowCount);
        List<Ld_Member_Account> GetAccountPagingPro(int systemId, string companyId, string delete, int pageId, int pageSize, out int rowCount);
        List<Ld_Member_Account> SearchAccountPro(int systemId, string companyId, string startTime, string endTime, string classId, string rankId, string keyword, out int rowCount);
        List<Ld_Member_Account> SearchAccountPro(int systemId, string companyId, string startTime, string endTime, string classId, string rankId, string keyword, string delete, out int rowCount);
        List<Ld_Member_Account> SearchAccountTopPro(int systemId, string companyId, string startTime, string endTime, string classId, string rankId, string keyword, int count, out int rowCount);
        List<Ld_Member_Account> SearchAccountTopPro(int systemId, string companyId, string startTime, string endTime, string classId, string rankId, string keyword, string delete, int count, out int rowCount);
        List<Ld_Member_Account> SearchAccountPagingPro(int systemId, string companyId, string startTime, string endTime, string classId, string rankId, string keyword, int pageId, int pageSize, out int rowCount);
        List<Ld_Member_Account> SearchAccountPagingPro(int systemId, string companyId, string startTime, string endTime, string classId, string rankId, string keyword, string delete, int pageId, int pageSize, out int rowCount);

        bool VerifyAccountLoginPro(int systemId, string companyId, string account, string password);

    }
}
