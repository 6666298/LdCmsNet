﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Member
{
    using EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    public partial interface IAddressService:IBaseService<Ld_Member_Address>
    {
        bool SaveAddress(Ld_Member_Address entity);
        bool UpdateAddress(Ld_Member_Address entity);
        bool UpdateAddressDefault(int systemId, string companyId, string addressId, bool isDefault);
        bool UpdateAddressState(int systemId, string companyId, string addressId, bool state);
        bool DeleteAddress(int systemId, string companyId, string addressId);
        Ld_Member_Address GetAddress(int systemId, string companyId, string addressId);
        Ld_Member_Address GetAddressByDefault(int systemId, string companyId);
        List<Ld_Member_Address> GetAddressAll(int systemId, string companyId);
        List<Ld_Member_Address> GetAddressTop(int systemId, string companyId, int count);
        List<Ld_Member_Address> GetAddressPaging(int systemId, string companyId, int pageId, int pageSize);
        List<Ld_Member_Address> SearchAddress(int systemId, string companyId, string startTime, string endTime, string keyword, int count);
        List<Ld_Member_Address> GetAddressPagingByMemberId(int systemId, string companyId, string memberId, int pageId, int pageSize);
        int CountAddress(int systemId, string companyId);
        int CountAddress(int systemId, string companyId, string memberId);
        int CountAddress(int systemId, string companyId, string startTime, string endTime, string keyword);


    }
}
