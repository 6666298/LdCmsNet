﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Member
{
    using LdCms.EF.DbModels;
    public partial interface IClassifyService:IBaseService<Ld_Member_Classify>
    {
        bool IsExists(int systemId, string companyId, int classId);
        bool SaveClassify(Ld_Member_Classify entity);
        bool UpdateClassify(Ld_Member_Classify entity);
        bool UpdateClassifyState(int systemId, string companyId, int classId, bool state);
        bool DeleteClassify(int systemId, string companyId, int classId);
        bool DeleteClassify(int systemId, string companyId, int[] classId);
        Ld_Member_Classify GetClassify(int systemId, string companyId, int classId);
        List<Ld_Member_Classify> GetClassify(int systemId, string companyId);
        List<Ld_Member_Classify> GetClassify(int systemId, string companyId, bool state);
        int CountClassify(int systemId, string companyId);
        int CountClassify(int systemId, string companyId, bool state);

    }
}
