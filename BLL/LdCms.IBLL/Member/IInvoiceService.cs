﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Member
{
    using EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    public partial interface IInvoiceService:IBaseService<Ld_Member_Invoice>
    {
        bool SaveInvoice(Ld_Member_Invoice entity);
        bool UpdateInvoice(Ld_Member_Invoice entity);
        bool UpdateInvoiceDefault(int systemId, string companyId, string invoiceId, bool isDefault);
        bool UpdateInvoiceState(int systemId, string companyId, string invoiceId, bool state);
        bool DeleteInvoice(int systemId, string companyId, string invoiceId);
        Ld_Member_Invoice GetInvoice(int systemId, string companyId, string invoiceId);
        Ld_Member_Invoice GetInvoiceByDefault(int systemId, string companyId);
        List<Ld_Member_Invoice> GetInvoiceAll(int systemId, string companyId);
        List<Ld_Member_Invoice> GetInvoiceTop(int systemId, string companyId, int count);
        List<Ld_Member_Invoice> GetInvoicePaging(int systemId, string companyId, int pageId, int pageSize);
        List<Ld_Member_Invoice> SearchInvoice(int systemId, string companyId, string startTime, string endTime, string classId, string keyword, int count);
        List<Ld_Member_Invoice> GetInvoicePagingByMemberId(int systemId, string companyId, string memberId, int pageId, int pageSize);
        int CountInvoice(int systemId, string companyId);
        int CountInvoice(int systemId, string companyId, string memberId);
        int CountInvoice(int systemId, string companyId, string startTime, string endTime, string classId, string keyword);


    }
}
