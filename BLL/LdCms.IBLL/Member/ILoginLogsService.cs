﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Member
{
    using EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    /// <summary>
    /// 
    /// </summary>
    public partial interface ILoginLogsService:IBaseService<Ld_Member_LoginLogs>
    {
        bool SaveLoginLogs(Ld_Member_LoginLogs entity);
        bool DeleteLoginLogs(long id);
        bool DeleteLoginLogsAll(int systemId, string companyId);
        Ld_Member_LoginLogs GetLoginLogs(long id);
        List<Ld_Member_LoginLogs> GetLoginLogsTop(int systemId, string companyId, int count);
        List<Ld_Member_LoginLogs> GetLoginLogsPaging(int systemId, string companyId, int pageId, int pageSize);
        List<Ld_Member_LoginLogs> SearchLoginLogs(int systemId, string companyId, string startTime, string endTime, string clientId, string keyword, int count);
        int CountLoginLogs(int systemId, string companyId);


    }
}
