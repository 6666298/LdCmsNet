﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Member
{
    using LdCms.EF.DbModels;
    public partial interface ISettingsService:IBaseService<Ld_Member_Settings>
    {
        bool CreateSettings(int systemId, string companyId);
        bool UpdateSettings(Ld_Member_Settings entity);
        Ld_Member_Settings GetSettings(int systemId, string companyId);
    }
}
