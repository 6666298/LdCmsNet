﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.Common.Enum
{
    /// <summary>
    /// 
    /// </summary>
    public class ParamEnum
    {
        public enum TokenType
        {
            Sys = 1,
            AppID = 16,
            Uuid = 32,
            Member = 48,
            Staff = 64,
            Token = 128
        }
        public enum Sex
        {
            保密 = 0,
            男 = 1,
            女 = 2
        }
        public enum Client
        {
            Empty = 0,
            Web = 1,
            M = 2,
            WX = 3,
            App = 4
        }
        public enum Interface
        {
            后端=1,
            前端=2
        }
        public enum ColumnPermission
        {
            查看权限 = 1,
            发帖权限 = 2,
            回帖权限 = 3
        }
        public enum WeChatPublicPlatform
        {
            测试号 = 1,
            订阅号 = 2,
            服务号 = 3,
            小程序 = 4,
            企业号 = 5
        }
        public enum WeChatPayPlatform
        {
            普通商户版 = 1,
            服务商版 = 2,
            银行服务商版 = 3,
            沙箱仿真版 = 4,

        }
        public enum AliPayPlatform
        {
            沙箱仿真版 = 1,
            系统服务商ISV = 2
        }
        public enum AlipayPlatformAppType
        {
            小程序 = 1,
            生活号 = 2,
            网页移动应用 = 3,
            第三方应用 = 4
        }
        public enum MemberType
        {
            个人 = 1,
            业务 = 2,
            公司 = 3
        }
        public enum MemberPointClass
        {
            新人 = 1,
            登录 = 2,
            签到 = 3,
            订单 = 4,
            评价 = 5,
            操作 = 6
        }
        public enum MemberPointType
        {
            新增 = 1,
            使用 = 2
        }
        public enum InvoiceCategory
        {
            普通发票 = 1,
            专用发票 = 2
        }
        public enum InvoiceType
        {
            纸质发票=1,
            电子发票=2
        }
        public enum InvoiceStatus
        {
            已申请=0,
            已受理=1,
            已开票=2
        }
        public enum CommentServiceStatus
        {
            差评=1,
            中评=2,
            好评=3
        }
        public enum PayChannel
        {
            WEIXIN,
            ALIPAY
        }
        public enum OrderStatus
        {
            新订单=0,
            已支付=1,
            已发货=2,
            已完成=3,
            已取消=4,
            已关闭=5
        }


    }
}
