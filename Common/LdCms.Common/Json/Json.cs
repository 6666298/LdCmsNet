﻿using System.Collections.Generic;
using System.Data;

namespace LdCms.Common.Json
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Converters;
    using System;

    /// <summary>
    /// Json 操作类
    /// 
    /// 作者：小草
    /// 功能：
    ///     1
    /// 
    /// </summary>
    public static class Json
    {
        public static bool IsJson(this string str)
        {
            try
            {
                JObject.Parse(str);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsJsonNode(this string str, string node)
        {
            try
            {
                JObject jo = JObject.Parse(str);
                if (jo.Property(node) == null || jo.Property(node).ToString() == "")
                    return false;
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }
        public static string JsonClear(this string str)
        {
            try
            {
                string result = str.Replace(" ", "");
                result = result.Replace("\n", "");
                result = result.Replace("\t", "");
                result = result.Replace("\r", "");
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static string GetJsonNodeValue(this string str, string node)
        {
            try
            {
                if (!str.IsJson())
                    return string.Empty;
                JObject jo = JObject.Parse(str);
                if (jo.Property(node) == null || jo.Property(node).ToString() == "")
                    return string.Empty;
                return (string)jo[node];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static object ToJson(this string json)
        {
            return json == null ? null : JsonConvert.DeserializeObject(json);
        }
        public static string ToJson(this object obj)
        {
            JsonSerializerSettings jsonSetting = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                DateFormatString = "yyyy-MM-dd HH:mm:ss"
            };
            return JsonConvert.SerializeObject(obj, Formatting.None, jsonSetting);
        }
        public static string ToJson(this object obj, string datetimeformats)
        {
            var timeConverter = new IsoDateTimeConverter { DateTimeFormat = datetimeformats };
            return JsonConvert.SerializeObject(obj, timeConverter);
        }
        public static T ToObject<T>(this string json)
        {
            return json == null ? default(T) : JsonConvert.DeserializeObject<T>(json);
        }
        public static T ToObject<T>(this object obj)
        {
            string json = obj.ToJson();
            return json == null ? default(T) : JsonConvert.DeserializeObject<T>(json);
        }
        public static List<T> ToList<T>(this string json)
        {
            return json == null ? null : JsonConvert.DeserializeObject<List<T>>(json);
        }
        public static DataTable ToTable(this string json)
        {
            return json == null ? null : JsonConvert.DeserializeObject<DataTable>(json);
        }
        public static JObject ToJObject(this string json)
        {
            return json == null ? JObject.Parse("{}") : JObject.Parse(json.Replace("&nbsp;", ""));
        }


       

    }



}
