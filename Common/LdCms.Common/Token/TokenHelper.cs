﻿using System;

namespace LdCms.Common.Token
{
    using LdCms.Common.Security;
    using LdCms.Common.Time;

    /// <summary>
    /// 接口Token帮助类
    /// </summary>
    public class TokenHelper
    {
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public string Token { get; set; }
        public TokenHelper() { }
        public TokenHelper(string token) : this("", "", token) { }
        public TokenHelper(string appId, string appSecret) : this(appId, appSecret, "") { }
        public TokenHelper(string appId, string appSecret, string token)
        {
            AppId = appId;
            AppSecret = appSecret;
            Token = token;
        }
        public string GetToken()
        {
            return CreateToken(0,0);
        }
        public string GetToken(int length)
        {
            try
            {
                return CreateToken(length, 0);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string GetToken(int length, int total)
        {
            try
            {
                return CreateToken(length, total);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IsToken(int createTimestamp, int expiresIn)
        {
            try
            {
                int indexTimestamp = TimeHelper.GetUnixTimestamp();
                createTimestamp = Math.Abs(createTimestamp);
                expiresIn = Math.Abs(expiresIn);
                if ((indexTimestamp - createTimestamp) < expiresIn)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private string CreateToken(int length, int total)
        {
            try
            {
                string strGuid = GeneralCodeHelper.GetGuid(GeneralCodeHelper.GuIdFormat.N);                 //32
                string intGuid = GeneralCodeHelper.GuidTo16String().PadLeft(16, '0');                       //16
                string aRandomStr = GeneralCodeHelper.RandomString(16, true, true, true, true);             //16
                if (length == 0 || length == 48)
                    return string.Format("{0}{1}", strGuid, intGuid);
                if (length == 64)
                    return string.Format("{0}{1}{2}", strGuid, intGuid, aRandomStr);

                //补位
                int totalNumber = total <= 0 ? 1 : total + 1;
                int totalLength = length <= 64 ? 80 : length;
                int randomLength = (totalLength - 64 - 1) - totalNumber.ToString().Length;
                string bRandomStr = GeneralCodeHelper.RandomString(randomLength, true, true, true, true);   //补位
                return string.Format("{0}_{1}{2}{3}{4}", totalNumber, strGuid, intGuid, aRandomStr, bRandomStr);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
