﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.Common.Utility
{
    public partial class Utility
    {
        public static string GetInnerException(Exception exception)
        {
            try
            {
                return exception.InnerException == null ? string.Empty : exception.InnerException.Message.Replace("\r", "");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
