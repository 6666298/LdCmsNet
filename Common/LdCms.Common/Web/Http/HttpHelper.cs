﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace LdCms.Common.Web
{
    using LdCms.Common.Web.Http;
    /// <summary>
    /// 
    /// </summary>
    public class HttpHelper
    {
        private static HttpRequestUtilities t = new HttpRequestUtilities();
        public static string RequestInputStream()
        {
            return t.RequestInputStream();
        }
        public static string RequestInputStream(string codeType)
        {
            t.CodeType = codeType;
            return t.RequestInputStream();
        }
        public static string GetToUrl(string url)
        {
            return t.GetToUrl(url);
        }
        public static string GetToUrl(string url, string codeType)
        {
            return t.GetToUrl(url, codeType);
        }

        public static string PostToUrl(string url)
        {
            return t.PostToUrl(url);
        }
        public static string PostToUrl(string url, string data)
        {
            return t.PostToUrl(url, data);
        }
        public static string PostToUrl(string url, string data,string codeType)
        {
            t.CodeType = codeType;
            return t.PostToUrl(url, data);
        }
        public static string PostToUrl(string url, HttpRequestEnum.ContentType contentType, string data)
        {
            return t.PostToUrl(url, contentType, data);
        }
        public static string PostToUrl(string url, HttpRequestEnum.ContentType contentType, string data, string codeType)
        {
            t.CodeType = codeType;
            return t.PostToUrl(url, contentType, data);
        }
        public static string PostToUrl(string url, X509Certificate2 cert, string data)
        {
            return t.PostToUrl(url, cert, data);
        }
        public static string PostToUrl(string url, X509Certificate2 cert, string data, string codeType)
        {
            t.CodeType = codeType;
            return t.PostToUrl(url, cert, data);
        }

        public static string PutToUrl(string url, string data)
        {
            return t.PutToUrl(url, data);
        }
        public static string PutToUrl(string url, string data, string codeType)
        {
            t.CodeType = codeType;
            return t.PutToUrl(url, data);
        }
        public static string PutToUrl(string url, HttpRequestEnum.ContentType contentType, string data)
        {
            return t.PutToUrl(url, contentType, data);
        }
        public static string PutToUrl(string url, HttpRequestEnum.ContentType contentType, string data, string codeType)
        {
            t.CodeType = codeType;
            return t.PutToUrl(url, contentType, data);
        }

        public static string PatchToUrl(string url, string data)
        {
            return t.PatchToUrl(url, data);
        }
        public static string PatchToUrl(string url, string data, string codeType)
        {
            t.CodeType = codeType;
            return t.PatchToUrl(url, data);
        }
        public static string PatchToUrl(string url, HttpRequestEnum.ContentType contentType, string data)
        {
            return t.PatchToUrl(url, contentType, data);
        }
        public static string PatchToUrl(string url, HttpRequestEnum.ContentType contentType, string data, string codeType)
        {
            t.CodeType = codeType;
            return t.PatchToUrl(url, contentType, data);
        }

        public static string DeleteToUrl(string url)
        {
            return t.DeleteToUrl(url);
        }
        public static string DeleteToUrl(string url, string codeType)
        {
            t.CodeType = codeType;
            return t.DeleteToUrl(url);
        }

        public static string GetFileToUrl(string url, string savePath)
        {
            return t.GetFileToUrl(url, savePath);
        }
        public static string PostFileToUrl(string url, string filePath)
        {
            return t.PostFileToUrl(url, filePath);
        }
        public static string PostFileToUrl(string url, string headerName, string filePath)
        {
            return t.PostFileToUrl(url, headerName, filePath);
        }
        public static string PostFileToUrl(string url, string data, string headerName, string filePath)
        {
            return t.PostFileToUrl(url, data, headerName, filePath);
        }

        public static string HttpRequest(string url, HttpRequestEnum.Method method, HttpRequestEnum.ContentType contentType)
        {
            return t.HttpRequest(url, method, contentType);
        }
        public static string HttpRequest(string url, HttpRequestEnum.Method method, HttpRequestEnum.ContentType contentType, string data)
        {
            return t.HttpRequest(url, method, contentType, data);
        }
        public static string HttpRequest(string url, HttpRequestEnum.Method method, HttpRequestEnum.ContentType contentType, string data, string codeType)
        {
            return t.HttpRequest(url, method, contentType, data, codeType);
        }
        public static HttpWebResponse HttpResponse(string url, HttpRequestEnum.Method method, HttpRequestEnum.ContentType contentType, string data, string codeType)
        {
            return t.HttpResponse(url, method, contentType, data, codeType);
        }

    }
}
