﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LdCms.Common.Web.Http
{
    public class HttpRequestEnum
    {
        public enum Method
        {
            /// <summary>
            /// 获取服务器支持的HTTP请求方法
            /// </summary>
            OPTIONS,
            /// <summary>
            /// 用于检查对象是否存在，并获取包含在响应消息头中的信息。
            /// </summary>
            HEAD,
            /// <summary>
            /// 向特定的资源发出请求，得到资源。
            /// </summary>
            GET,
            /// <summary>
            /// 向指定资源提交数据进行处理的请求，用于添加新的内容。
            /// </summary>
            POST,
            /// <summary>
            /// 向指定资源位置上传其最新的内容，用于修改某个内容。
            /// </summary>
            PUT,
            /// <summary>
            /// 是对PUT方法的补充，用来对已知资源进行局部更新
            /// </summary>
            PATCH,
            /// <summary>
            /// 请求服务器删除请求的URI所标识的资源，用于删除。
            /// </summary>
            DELETE,
            /// <summary>
            /// 回馈服务器收到的请求，用于远程诊断服务器。
            /// </summary>
            TRACE,
            /// <summary>
            /// CT用于代理进行传输，如使用ssl
            /// </summary>
            CONNECT
        }
        public enum ContentType
        {
            /// <summary>
            /// POST内容 url编码类型,默认POST数据类型 application/x-www-form-urlencoded
            /// </summary>
            [Description("application/x-www-form-urlencoded")]
            UrlEncoded,
            /// <summary>
            /// POST文件 上传文件类型  multipart/form-data
            /// </summary>
            [Description("multipart/form-data")]
            Data,
            /// <summary>
            /// JSON 格式 application/json
            /// </summary>
            [Description("application/json")]
            Json,
            /// <summary>
            /// XML格式 text/xml
            /// </summary>
            [Description("text/xml")]
            Xml,
            /// <summary>
            /// html格式 text/html
            /// </summary>
            [Description("text/html")]
            Html
        }
    }

}
