﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace LdCms.DAL
{
    using LdCms.IDAL;
    using LdCms.EF.DbContext;

    /// <summary>
    /// DAL操作实现
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public partial class BaseDAL : IBaseDAL
    {
        private LdCmsDbEntitiesContext dbContext = DbContextFactory.GetCurrentContext();
        public LdCmsDbEntitiesContext DbEntities()
        {
            return dbContext;
        }
        public void Add<T>(T t) where T : class
        {
            dbContext.Set<T>().Add(t);
        }
        public void Add<T>(List<T> entitys) where T : class
        {
            foreach (var entity in entitys)
            {
                dbContext.Entry<T>(entity).State = EntityState.Added;
            }
        }
        public void Update<T>(T t) where T : class
        {
            dbContext.Set<T>().AddOrUpdate(t);
        }
        public void Update<T>(List<T> entitys) where T : class
        {
            foreach (var entity in entitys)
            {
                dbContext.Entry<T>(entity).State = EntityState.Modified;
            }
        }
        public void Delete<T>(T t) where T : class
        {
            dbContext.Set<T>().Remove(t);
        }
        public void Delete<T>(Expression<Func<T, bool>> whereLambda) where T : class
        {
            var entitys = dbContext.Set<T>().Where(whereLambda).ToList();
            entitys.ForEach(m => dbContext.Entry<T>(m).State = EntityState.Deleted);
        }
        public bool IsExists<T>(Expression<Func<T, bool>> whereLambda) where T : class
        {
            return dbContext.Set<T>().Any(whereLambda);
        }
        public int Count<T>(Expression<Func<T, bool>> whereLambda) where T : class
        {
            return dbContext.Set<T>().Count(whereLambda);
        }
        public int ExecuteCommand(string cmdText)
        {
            return dbContext.Database.ExecuteSqlCommand(cmdText);
        }
        public int ExecuteCommand(string cmdText, DbParameter[] sqlParameter)
        {
            return dbContext.Database.ExecuteSqlCommand(cmdText, sqlParameter);
        }
        public T Find<T>(params object[] keyValues) where T : class
        {
            return dbContext.Set<T>().Find(keyValues);
        }
        public T Find<T>(Expression<Func<T, bool>> whereLambda) where T : class
        {
            return dbContext.Set<T>().FirstOrDefault<T>(whereLambda);
        }
        public List<T> FindList<T>(string cmdText) where T : class
        {
            return dbContext.Set<T>().SqlQuery(cmdText).ToList<T>();
        }
        public List<T> FindList<T>(string cmdText, DbParameter[] sqlParameter) where T : class
        {
            return dbContext.Set<T>().SqlQuery(cmdText, sqlParameter).ToList<T>();
        }
        public IQueryable<T> FindList<T>() where T : class
        {
            return dbContext.Set<T>();
        }
        public IQueryable<T> FindList<T>(Expression<Func<T, bool>> whereLambda) where T : class
        {
            return dbContext.Set<T>().Where(whereLambda);
        }
        public IQueryable<T> FindList<T>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, bool>> orderLambda, bool isAsc) where T : class
        {
            if(isAsc)
                return dbContext.Set<T>().Where(whereLambda).OrderBy(orderLambda);
            else
                return dbContext.Set<T>().Where(whereLambda).OrderByDescending(orderLambda);
        }
        public IQueryable<T> FindListTop<T>(Expression<Func<T, bool>> whereLambda, int count) where T : class
        {
            return dbContext.Set<T>().Take(count).Where(whereLambda);
        }
        public IQueryable<T> FindListTop<T>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, bool>> orderLambda, bool isAsc, int count) where T : class
        {
            if (isAsc)
                return dbContext.Set<T>().Take(count).Where(whereLambda).OrderBy(orderLambda);
            else
                return dbContext.Set<T>().Take(count).Where(whereLambda).OrderByDescending(orderLambda);
        }
        public IQueryable<T> FindListTop<T>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, T>> scalarLambda, int count) where T : class
        {
            return dbContext.Set<T>().Take(count).AsNoTracking().Where(whereLambda).Select(scalarLambda);
        }
        public IQueryable<T> FindListTop<T>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, T>> scalarLambda, Expression<Func<T, bool>> orderLambda, bool isAsc, int count) where T : class
        {
            if (isAsc)
                return dbContext.Set<T>().Take(count).AsNoTracking().Where(whereLambda).OrderBy(orderLambda).Select(scalarLambda);
            else
                return dbContext.Set<T>().Take(count).AsNoTracking().Where(whereLambda).OrderByDescending(orderLambda).Select(scalarLambda);
        }

        public IQueryable<T> FindListPaging<T>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, bool>> orderLambda, bool isAsc, int pageIndex, int pageSize) where T : class
        {
            if (isAsc)
                return dbContext.Set<T>().Skip((pageIndex - 1) * pageSize).Take(pageSize).Where(whereLambda).OrderBy(orderLambda);
            else
                return dbContext.Set<T>().Skip((pageIndex - 1) * pageSize).Take(pageSize).Where(whereLambda).OrderByDescending(orderLambda);
        }
        public IQueryable<T> FindListPaging<T>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, T>> scalarLambda, Expression<Func<T, bool>> orderLambda, bool isAsc, int pageIndex, int pageSize) where T : class
        {
            if (isAsc)
                return dbContext.Set<T>().Skip((pageIndex - 1) * pageSize).Take(pageSize).Where(whereLambda).OrderBy(orderLambda).Select(scalarLambda);
            else
                return dbContext.Set<T>().Skip((pageIndex - 1) * pageSize).Take(pageSize).Where(whereLambda).OrderByDescending(orderLambda).Select(scalarLambda);
        }

        public bool SaveChanges()
        {
            return dbContext.SaveChanges() > 0;
        }
    }
}
