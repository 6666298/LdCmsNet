﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IDAL.Member
{
    using EF.DbModels;

    public partial interface IAccountAccessTokenDAL:IBaseDAL<Ld_Member_AccountAccessToken>
    {
    }
}
