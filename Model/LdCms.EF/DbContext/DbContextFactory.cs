﻿using System.Runtime.Remoting.Messaging;

namespace LdCms.EF.DbContext
{
    /// <summary>
    /// 上下文简单工厂
    /// </summary>
    public class DbContextFactory
    {
        /// <summary>
        /// 获取当前数据上下文
        /// </summary>
        /// <returns></returns>
        public static LdCmsDbEntitiesContext GetCurrentContext()
        {
            LdCmsDbEntitiesContext dbContext = CallContext.GetData("LdCmsDbContext") as LdCmsDbEntitiesContext;
            if (dbContext == null)
            {
                dbContext = new LdCmsDbEntitiesContext();
                CallContext.SetData("LdCmsDbContext", dbContext);
            }
            return dbContext;
        }
    }
}
