﻿using System;
using System.Collections;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;

namespace LdCms.EF.DbContext
{
    using LdCms.EF.DbModels;
    /// <summary>
    /// TestDbExContext继承TestDBContext，而TestDBContext又继承DbContext
    /// </summary>
    public partial class LdCmsDbEntitiesContext : LdCmsDbContext
    {
        public LdCmsDbEntitiesContext(){ }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //先调用基类的OnModelCreating方法，设置数据库中其它表和实体的映射关系
            base.OnModelCreating(modelBuilder);
            //接着设置实体V_Person和数据库中V_Person视图的关系
            //媒体资源文件视图表
            modelBuilder.Entity<V_Basics_Media>();

        }

    }
}
