﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace LdCms.EF.DbContext
{
    /// <summary>
    /// 
    /// </summary>
    public partial class LdCmsDbEntitiesContext
    {
        public int SP_Add_Member_PointRecord(int systemId, string companyId, string memberId, int classId, string className, int typeId, string typeName, int points, string tasksId,string body,string account,string nickname, string ipAddress,string remark,bool state, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Add_Member_PointRecord";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.NVarChar,20),
                    new SqlParameter("@memberId", SqlDbType.VarChar,20),
                    new SqlParameter("@classId", SqlDbType.Int,4),
                    new SqlParameter("@className", SqlDbType.NVarChar,20),
                    new SqlParameter("@typeId", SqlDbType.Int,4),
                    new SqlParameter("@typeName", SqlDbType.NVarChar,20),
                    new SqlParameter("@points", SqlDbType.Int,8),
                    new SqlParameter("@tasksId", SqlDbType.VarChar,64),
                    new SqlParameter("@body", SqlDbType.NVarChar,1000),
                    new SqlParameter("@account", SqlDbType.VarChar,20),
                    new SqlParameter("@nickname", SqlDbType.NVarChar,20),
                    new SqlParameter("@ipAddress", SqlDbType.VarChar,20),
                    new SqlParameter("@remark", SqlDbType.NVarChar,400),
                    new SqlParameter("@state", SqlDbType.Bit,4),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = memberId;
                param[3].Value = classId;
                param[4].Value = className;
                param[5].Value = typeId;
                param[6].Value = typeName;
                param[7].Value = points;
                param[8].Value = tasksId;
                param[9].Value = body;
                param[10].Value = account;
                param[11].Value = nickname;
                param[12].Value = ipAddress;
                param[13].Value = remark;
                param[14].Value = state;
                param[15].Direction = ParameterDirection.Output;
                param[16].Direction = ParameterDirection.Output;
                var result = this.ExecuteNonQueryPro(cmdText, param);
                errorCode = (int)param[15].Value;
                errorMsg = (string)param[16].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
