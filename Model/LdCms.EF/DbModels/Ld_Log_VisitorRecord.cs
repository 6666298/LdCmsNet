//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace LdCms.EF.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ld_Log_VisitorRecord
    {
        public long ID { get; set; }
        public Nullable<int> SystemID { get; set; }
        public string CompanyID { get; set; }
        public Nullable<byte> ClientID { get; set; }
        public string IpAddress { get; set; }
        public string Host { get; set; }
        public string AbsoluteUri { get; set; }
        public string QueryString { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}
