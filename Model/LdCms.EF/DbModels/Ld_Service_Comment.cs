//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace LdCms.EF.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ld_Service_Comment
    {
        public int SystemID { get; set; }
        public string CompanyID { get; set; }
        public string CommentID { get; set; }
        public string MemberID { get; set; }
        public string OrderID { get; set; }
        public Nullable<int> ServiceStatus { get; set; }
        public Nullable<int> ServicePoints { get; set; }
        public string Images { get; set; }
        public string Description { get; set; }
        public Nullable<int> UpNum { get; set; }
        public Nullable<int> DownNum { get; set; }
        public Nullable<bool> State { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}
