# LdCms.Net

#### 系统介绍
LdCms.Net 是使用Net4.7.2 ASP.NET MVC5 + Autofac 开发一个轻量级的网站信息管理系统。

ASP.NET CORE 版 https://gitee.com/6666298/LdCmsNetCore

管理后台UI使用：H-ui.admin。UI官网：http://www.h-ui.net

> 注意：正在开发中，不建议直接使用，近期会一直更新、完善。请多多关注。谢谢！

#### QQ群：672575385

测试地址：[http://net.ldcms.net/admin/login?companyid=300002](http://net.ldcms.net/admin/login?companyid=300002)

帐号：admin
密码：admin

![输入图片说明](https://images.gitee.com/uploads/images/2019/1210/173539_0d3e9705_1201395.png "我的桌面.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/123418_aad9d91b_1201395.png "QQ图片20190402123336.png")

###  **技术交流群** 

![输入图片说明](https://images.gitee.com/uploads/images/2020/0425/104349_780f47f9_1201395.jpeg "002.jpg")

###  **捐赠** 

如果您发现对你很有用，可以请我们喝杯茶！
