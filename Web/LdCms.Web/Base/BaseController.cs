﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace LdCms.Web
{
    using LdCms.Web.Models;
    using LdCms.Common.Utility;
    using LdCms.Common.Extension;
    using LdCms.Common.Json;
    using LdCms.Common.Web;
    using LdCms.Common.Security;
    using LdCms.Web.Services;
    using System.Web.Mvc;
    using LdCms.Common.Net;
    using LdCms.Web.Framework;

    /// <summary>
    /// MVC 基础控制器
    /// </summary>
    [ExceptionHandlerAttribute]
    public class BaseController : Controller
    {
        private readonly IBaseManager BaseManager;
        public BaseController(IBaseManager BaseManager)
        {
            this.BaseManager = BaseManager;
            this.SystemID = BaseSystemConfig.SystemID;
            this.SessionName = BaseSystemConfig.SessionName;
            InitLoginData();
        }

        protected string SessionName;
        protected int SystemID;
        protected string CompanyID;
        protected string CompanyName;
        protected string StaffID;
        protected string StaffName;
        protected string WeChatServerNotifyUrl = BaseSystemConfig.WeChatServerNotifyUrl;
        protected string AlipayServerCheckUrl = BaseSystemConfig.AlipayServerCheckUrl;

        public void InitLoginData()
        {
            string sessionJson = WebHelper.GetCookie(SessionName);
            AccountModel loginStaffModel = DESEncryptHelper.DecryptDES(sessionJson).ToObject<AccountModel>();
            if (loginStaffModel != null)
            {
                CompanyID = loginStaffModel.CompanyID;
                CompanyName = loginStaffModel.CompanyName;
                StaffID = loginStaffModel.StaffID;
                StaffName = loginStaffModel.StaffName;
            }
        }
        public SiteConfig SiteConfig
        {
            get
            {
                var content = FileHelper.Open(BaseSystemConfig.SiteConfigFile);
                return content.ToObject<SiteConfig>();
            }
        }

        public virtual ActionResult Index()
        {
            return View();
        }
        protected virtual JsonResult Result(object data)
        {
            return ToJsonResult(data);
        }
        protected virtual JsonResult Success(string message)
        {
            var result = new { state = "success", message };
            return ToJsonResult(result);
        }
        protected virtual JsonResult Success(string message, object data)
        {
            var result = new { state = "success",  message,  data };
            return ToJsonResult(result);
        }
        protected virtual JsonResult Error(string message)
        {
            var result = new { state = "error", message };
            return ToJsonResult(result);
        }
        protected virtual ActionResult ToError(string message)
        {
            return RedirectToAction("Show", "Error", new { errcode = -1, errmsg = message });
        }
        protected virtual ActionResult ToPermission(string funcId)
        {
            return RedirectToAction("Permission", "Error", new { funcId });
        }

        protected bool IsPermission(string functionId)
        {
            try
            {
                return BaseManager.IsPermission(CompanyID, StaffID, functionId);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private JsonResult ToJsonResult(object data)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #region 私有化共用方法
        protected string GetQueryString(string name)
        {
            try
            {
                return BaseManager.GetQueryString(name);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        protected string GetFormValue(string name)
        {
            try
            {
                return BaseManager.GetFormValue(name);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        protected string GetFormValueArr(string name)
        {
            try
            {
                return BaseManager.GetFormValueArr(name);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        protected string GetFormValue(FormCollection formValue, string name)
        {
            try
            {
                return BaseManager.GetFormValue(formValue, name);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


    }
}