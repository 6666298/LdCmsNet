﻿using System;
using System.Web.Mvc;

namespace LdCms.Web.Controllers
{
    using LdCms.Web.Services;
    /// <summary>
    /// 错误页
    /// </summary>
    public class ErrorController : BaseController
    {
        private readonly IBaseManager BaseManager;
        public ErrorController(IBaseManager BaseManager) : base(BaseManager)
        {
            this.BaseManager = BaseManager;
        }
        public override ActionResult Index()
        {
            return View();
        }
        public ActionResult Show(int errCode, string errMsg)
        {
            try
            {
                ViewData["ErrCode"] = errCode;
                ViewData["ErrMsg"] = errMsg;
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public ActionResult Permission(string funcId)
        {
            try
            {
                ViewData["FuncID"] = string.Format("系统功能编号：{0}", funcId);
                ViewData["Message"] = "您没有操作权限，请联系系统管理员！";
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public ActionResult Developing()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}