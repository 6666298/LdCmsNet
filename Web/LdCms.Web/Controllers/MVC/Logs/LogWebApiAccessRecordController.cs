﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LdCms.Web.Controllers.MVC.Logs
{
    using LdCms.EF.DbModels;
    using LdCms.IBLL.Log;
    using LdCms.Web.Models;
    using LdCms.Web.Services;
    /// <summary>
    /// 
    /// </summary>
    [AdminAuthorize(Roles = "Admins")]
    public class LogWebApiAccessRecordController : BaseController
    {
        private readonly IBaseManager BaseManager;
        private readonly IWebApiAccessRecordService WebApiAccessRecordService;
        public LogWebApiAccessRecordController(IBaseManager BaseManager, IWebApiAccessRecordService WebApiAccessRecordService) : base(BaseManager)
        {
            this.BaseManager = BaseManager;
            this.WebApiAccessRecordService = WebApiAccessRecordService;
        }
        public override ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.列表);
                if (!IsPermission(funcId)) { return ToPermission(funcId); }
                   
                string startTime = GetQueryString("datemin");
                string endTime = GetQueryString("datemax");
                string classId = GetQueryString("classId");
                string state = GetQueryString("state");
                string keyword = GetQueryString("keyword");
                ViewBag.DateMin = startTime;
                ViewBag.DateMax = endTime;
                ViewBag.ClassID = classId;
                ViewBag.State = state;
                ViewBag.Keyword = keyword;

                int total = 100;
                List<Ld_Log_WebApiAccessRecord> lists = new List<Ld_Log_WebApiAccessRecord>();
                if (string.IsNullOrWhiteSpace(keyword) && string.IsNullOrWhiteSpace(startTime))
                {
                    lists = WebApiAccessRecordService.GetWebApiAccessRecordTop(SystemID, CompanyID, total);
                    ViewBag.Count = WebApiAccessRecordService.CountWebApiAccessRecord(SystemID, CompanyID);
                }
                else
                {
                    lists = WebApiAccessRecordService.SearchWebApiAccessRecord(SystemID, CompanyID, startTime, endTime, classId, state, keyword, total);
                    ViewBag.Count = WebApiAccessRecordService.CountWebApiAccessRecord(SystemID, CompanyID, startTime, endTime, classId, state, keyword);
                }
                return View(lists);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }
        public ActionResult Show(long id)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.列表);
                if (!IsPermission(funcId)) { return Error("您没有操作权限，请联系系统管理员！"); }
                var entity = WebApiAccessRecordService.GetWebApiAccessRecord(id);
                return View(entity);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }


        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.删除);
                if (!IsPermission(funcId))
                    return Error("您没有操作权限，请联系系统管理员！");
                var entity = WebApiAccessRecordService.GetWebApiAccessRecord(id);
                if (entity == null)
                    return Error("id not exists！");
                bool result = WebApiAccessRecordService.DeleteWebApiAccessRecord(id);
                if (result)
                    return Success("成功！");
                else
                    return Error("失败！");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        [HttpPost]
        public ActionResult DeleteBatch(string[] arrId)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.删除);
                if (!IsPermission(funcId))
                    return Error("您没有操作权限，请联系系统管理员！");
                if (arrId.Length == 0)
                    return Error("请选择删除ID!");
                foreach (var item in arrId)
                {
                    long id = Convert.ToInt64(item);
                    bool result = WebApiAccessRecordService.DeleteWebApiAccessRecord(id);
                }
                return Success("成功！");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        [HttpPost]
        public ActionResult DeleteAll()
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.删除);
                if (!IsPermission(funcId))
                    return Error("您没有操作权限，请联系系统管理员！");
                bool result = WebApiAccessRecordService.DeleteWebApiAccessRecord(SystemID, CompanyID);
                if (result)
                    return Success("成功！");
                else
                    return Error("失败！");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }




    }
}