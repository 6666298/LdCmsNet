﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LdCms.Web.Controllers.MVC.Member
{
    using LdCms.EF.DbModels;
    using LdCms.IBLL.Member;
    using LdCms.Common.Extension;
    using LdCms.Web.Models;
    using LdCms.Web.Services;
    using LdCms.Common.Security;
    using LdCms.Common.Net;
    using LdCms.Common.Utility;
    using LdCms.Common.Json;
    /// <summary>
    /// 
    /// </summary>
    [AdminAuthorize(Roles = "Admins")]
    public class MemberPointRecordController : BaseController
    {
        private readonly IBaseManager BaseManager;
        private readonly IPointRecordService PointRecordService;
        public MemberPointRecordController(IBaseManager BaseManager, IPointRecordService PointRecordService) : base(BaseManager)
        {
            this.BaseManager = BaseManager;
            this.PointRecordService = PointRecordService;
        }


        public override ActionResult Index()
        {
            return View();
        }


        public ActionResult List()
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.会员管理.会员积分.列表);
                if (!IsPermission(funcId))
                    return ToPermission(funcId);
                string startTime = GetQueryString("datemin");
                string endTime = GetQueryString("datemax");
                string keyword = GetQueryString("keyword");
                ViewBag.DateMin = startTime;
                ViewBag.DateMax = endTime;
                ViewBag.Keyword = keyword;

                int total = 100;
                List<Ld_Member_PointRecord> lists = new List<Ld_Member_PointRecord>();
                string strKeyword = string.Format("{0}{1}", startTime, keyword);
                if (string.IsNullOrWhiteSpace(strKeyword))
                    lists = PointRecordService.GetPointRecordTop(SystemID, CompanyID, total);
                else
                    lists = PointRecordService.SearchPointRecord(SystemID, CompanyID, startTime, endTime, keyword, total);
                ViewBag.Count = PointRecordService.CountPointRecord(SystemID, CompanyID);
                return View(lists);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }


    }
}